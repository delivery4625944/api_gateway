package services

import (
	"delivery/api_gateway/config"
	"delivery/api_gateway/genproto/catalog_service"
	"delivery/api_gateway/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Catalog Service
	ProductService() catalog_service.ProductServiceClient
	CategoryService() catalog_service.CategoryServiceClient

	// User Service
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient
	UserService() user_service.UserServiceClient
	BranchService() user_service.BranchServiceClient
}

type grpcClients struct {
	// Catalog Service
	productService  catalog_service.ProductServiceClient
	categoryService catalog_service.CategoryServiceClient

	// User Service
	clientService  user_service.ClientServiceClient
	courierService user_service.CourierServiceClient
	userService    user_service.UserServiceClient
	branchService  user_service.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Catalog Microservice
	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// User Microservice
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	

	return &grpcClients{
			// Catalog Service
		productService:  catalog_service.NewProductServiceClient(connCatalogService),
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),

	// User Service
	clientService:  user_service.NewClientServiceClient(connUserService),
	courierService: user_service.NewCourierServiceClient(connUserService),
	userService:    user_service.NewUserServiceClient(connUserService),
	branchService:  user_service.NewBranchServiceClient(connUserService),
	}, nil
}

// Catalog Service
func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

// User Service
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}


